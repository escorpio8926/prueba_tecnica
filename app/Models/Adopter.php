<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adopter extends Model
{
	protected $table = 'adopters';

	protected $fillable=['name',
	'lastname',
	'address',
	'dni',
	'Adoption_date',
	'pet_id'];


}
