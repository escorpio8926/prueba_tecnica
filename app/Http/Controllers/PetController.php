<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Pet;
use Illuminate\Http\Request;

class PetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
     $pets= new Pet();

     $pets->name = $request->input('name');
     $pets->type = $request->input('type');
     $pets->age = $request->input('age');
     $pets->entry = $request->input('entry');

     $pets->save();
     return response()->json([
        'res' => true,
        'mensaje' => 'Mascota Ingresada correctamente',
        'status' => '200'
        ,$pets
    ]);
 }

 public function search_pet(Request $request)
 {
    if($request->has('id') && $request->has('name') && $request->has('type') )
    {
        $id=$request->id;
        $name=$request->name;
        $type=$request->type;
        if($id > 0){$identificador=' and '.$id.' = pet.id ';}else{$identificador='';}
        if(strlen($name) > 0){$nombre="and pet.name LIKE '%$name%'";}else{$nombre='';}
        if(strlen($type) > 0){$tipo="and pet.type LIKE '%$type%'";}else{$tipo='';}
        $pets =DB::select(DB::raw("
            SELECT
            pet.id
            ,pet.name
            ,pet.type
            ,pet.age
            ,pet.entry
            ,if( pa.pet_id >= 0, 'ADOPTADO', 'NO ADOPTADO')
            from prueba.pets as pet
            left join prueba.adopters as pa on pa.pet_id = pet.id 
            where 1 ".' '.$identificador .' '.$nombre.' '.$tipo));

    }

    return $pets;
}



/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
public function store(Request $request)
{
        //
}

/**
     * Display the specified resource.
     *
     * @param  \App\Models\Pet  $pet
     * @return \Illuminate\Http\Response
     */
public function show(Pet $pet)
{
        //
}

/**


/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pet  $pet
     * @return \Illuminate\Http\Response
     */
public function update(Request $request)
{
    $pet= Pet::find($request->id);
    $pet->name=$request->name;
    $pet->age=$request->age;
    $result=$pet->save();
    if($result)
        {return ["result"=>"Actualizado",'status' => '200'];}
    else
        {return ["result"=>"No Actualizado"];}
}

/**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pet  $pet
     * @return \Illuminate\Http\Response
     */
public function destroy(Pet $pet)
{
        //
}
}
