<?php

namespace App\Http\Controllers;

use App\Models\Adopter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdopterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $adopters= new Adopter();

        $adopters->name = $request->input('name');
        $adopters->lastname = $request->input('lastname');
        $adopters->address = $request->input('address');
        $adopters->phone = $request->input('phone');
        $adopters->dni = $request->input('dni');
        $adopters->Adoption_date = $request->input('Adoption_date');
        $adopters->pet_id  = $request->input('pet_id');

        $adopters->save();
        return response()->json([
            'res' => true,
            'mensaje' => 'Adoptante Ingresado correctamente',
            'status' => '200'
            ,$adopters
        ]);
    }

    public function search_adopter(Request $request)
    {
        if($request->has('buscar'))
        {
            $buscar=$request->buscar;
            if(strlen($buscar) <= 6 && strlen($buscar) >=1 ){$identificador=' ( '.$buscar.' = pa.id )';}else{$identificador='';}
            if(strlen($buscar) > 6 ){$documento=' ( '.$buscar.' = pa.dni )';}else{$documento='';}
            if(empty($buscar)){$empty=1;}else{$empty='';}

            $adopters =DB::select(DB::raw("
                SELECT *
                from prueba.adopters as pa
                where  $empty".' '.$identificador.' '.$documento));

        }

        return $adopters;
    }

    public function list()
    {
        return Adopter::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Adopter  $adopter
     * @return \Illuminate\Http\Response
     */
    public function show(Adopter $adopter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Adopter  $adopter
     * @return \Illuminate\Http\Response
     */
    public function edit(Adopter $adopter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Adopter  $adopter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $adopter= Adopter::find($request->id);
        $adopter->name=$request->name;
        $adopter->lastname=$request->lastname;
        $adopter->address=$request->address;
        $result=$adopter->save();
        if($result)
            {return ["result"=>"Actualizado",'status' => '200'];}
        else
            {return ["result"=>"No Actualizado"];}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Adopter  $adopter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Adopter $adopter)
    {
        //
    }
}
