<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PetController;
use App\Http\Controllers\AdopterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::post('/pet',[PetController::class,'create']); // ruta crear mascota
Route::post('/adopter',[AdopterController::class,'create']); // ruta crear adoptante
Route::get('/search_pet/', [PetController::class, 'search_pet']);// ruta buscar mascota
Route::get('/search_adopter/', [AdopterController::class, 'search_adopter']);// ruta buscar adoptante
Route::get('/adoption', [AdopterController::class, 'list']);// listado de adopciones
Route::put('/pet_update', [PetController::class, 'update']);// Actualización de mascota
Route::put('/adopter_update', [AdopterController::class, 'update']);// Actualización de adoptante
