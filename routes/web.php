<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('mascota');
});
Route::get('/adoptante', function () {
	return view('adoptante');
});
Route::get('/consultar_mascota', function () {
	return view('cmascota');
});

Route::get('/consultar_adoptante', function () {
	return view('cadoptante');
});

Route::get('/modificar_mascota', function () {
	return view('mmascota');
});

Route::get('/modificar_persona', function () {
	return view('mpersona');
});

