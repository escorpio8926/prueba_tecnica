<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>.padre {
      display: flex;
      justify-content: center;
  }
  body{
    background:url('{{ asset('2.jpg') }}');
    background-repeat: no-repeat;
    background-attachment: fixed;
    width: 1800px;
}</style>
<title>Mascotas Tucumán</title>
</head>
<body>

    <nav class="navbar navbar-light bg-light">

        <a href="http://127.0.0.1:8000/" class="btn btn-primary">Inicio</a>

    </nav>
    <div class="contanier my-5 padre" >

        <form id="formulario">

            <h3 style="text-align: center">Adopción</h3>
            <label for="name">name:</label>
            <input type="text" id="name" name="name" class="form-control" required><br><br>
            <label for="lastname">lastname:</label>
            <input type="text" id="lastname" name="lastname" class="form-control" required><br><br>
            <label for="address">address:</label>
            <input type="text" id="address" name="address" class="form-control" required><br><br>
            <label for="phone">phone:</label>
            <input type="text" id="phone" name="phone" class="form-control" required><br><br>
            <label for="dni">dni:</label>
            <input type="text" id="dni" name="dni" class="form-control" required><br><br>
            <label for="adoption_date">Adoption date:</label>
            <input type="date" id="adoption_date" name="adoption_date" class="form-control" required><br><br>
            <label for="pet_id">Pet Id:</label>
            <input type="number" id="pet_id" name="pet_id" class="form-control" required><br><br>
            <div style="text-align:center;">
                <input type="submit" value="Registrar">
            </div>
        </form>
    </div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
-->
<script>
    var formulario = document.getElementById('formulario');
    formulario.addEventListener('submit', function(e){
        e.preventDefault();
        var name = document.getElementById('name').value;
        var lastname = document.getElementById('lastname').value;
        var dni = document.getElementById('dni').value;
        var address = document.getElementById('address').value;
        var phone = document.getElementById('phone').value;
        var adoption_date = document.getElementById('adoption_date').value;
        var pet_id = document.getElementById('pet_id').value;
        fetch('http://127.0.0.1:8000/api/adopter', {
            method: 'POST',
            body: JSON.stringify({
                name:name,
                lastname:lastname,
                address:address,
                phone:phone,
                dni:dni,
                pet_id:pet_id,
                Adoption_date:adoption_date
                
            }),
            headers: {
                "Content-type": "application/json"}
            })
        .then(response => response.json())
        .then(json => console.log(json))
    })

</script>
</body>
</html>