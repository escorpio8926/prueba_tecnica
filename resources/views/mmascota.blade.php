<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>.padre {
      display: flex;
      justify-content: center;
  }
  body{
    background:url('{{ asset('10.png') }}');background-position: center;
    background-repeat: no-repeat;
    background-attachment: fixed;
    

}</style>
<title>Mascotas Tucumán</title>
</head>
<body>
    <nav class="navbar navbar-light bg-light">
      <form class="form-inline">
          <a href="http://127.0.0.1:8000/" class="btn btn-primary">Inicio</a>
      </form>
  </nav>
  <div class="contanier my-5 padre" >

    <form id="formulario">

        <h1>Modificar de Mascota</h1>
        <label for="id">Id:</label>
        <input type="number" id="id" name="id" class="form-control my-1" required><br><br>
        <label for="name">name:</label>
        <input type="text" id="name" name="name" class="form-control my-1" required><br><br>
        <label for="age">Age:</label>
        <input type="number" id="age" name="age" class="form-control my-1" required><br><br>
        <div style="text-align:center;">
            <input type="submit" value="Modificar">
        </div>
    </form>
</div>


<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
-->
<script>
    var formulario = document.getElementById('formulario');
    formulario.addEventListener('submit', function(e){
        e.preventDefault();
        var id = document.getElementById('id').value;
        var name = document.getElementById('name').value;
        var age = document.getElementById('age').value;

        fetch('http://127.0.0.1:8000/api/pet_update/', {
            method: 'PUT',
            body: JSON.stringify({
                name:name,
                id:id,
                age:age
            }),
            headers: {
                "Content-type": "application/json"}
            })
        .then(response => response.json())
        .then(json => console.log(json))
    })

</script>
</body>
</html>